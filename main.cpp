﻿#include "MultiplyBySeven.h"
#include "ReverseWords.h"
#include "SimulateLife.h"
#include "ListDuplicate.h"
#include <iostream>
#include <cassert>

#ifdef _DEBUG
	#include "MultiplyBySevenTest.h"
	#include "SimulateLifeTest.h"
	#include "ReverseWordsTest.h"
	#include "ListDuplicateTest.h"
#endif // _DEBUG

using namespace std;

int main(int argc, char** argv)
{

	#ifdef _DEBUG

		// Normally I would use a full Unit Testing Harness, but in interest of saving some time,
		// I have just seperated the "tests" into different files with asserts. Usually I would
		// set this up like a Unit Test where I would catch assertions and report the ones that
		// succeded when they should have failed, etc. I didn't because at that point I'm just
		// rewriting the wheel. If you would like this project with unit test harnesses, just ask!

		MultiplyBySevenTest();
		SimulateLifeTest();
		ReverseWordsTest();
		ListDuplicateTest();

		cout << "No asserts happened." << std::endl;

		// Normally I am using a Makefile with G++ (GNU C++), but because
		// VS2015 closes the window before you can view the output, I have this in the test project.
		getchar();

	#endif // _DEBUG


	return 0;
}
