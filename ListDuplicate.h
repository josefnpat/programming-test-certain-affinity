//*****************************************************************************
// Question: ListDuplicate
//
// Implement Node::LinkedListDuplicate() function in "ListDuplicate.cpp".
// See the .cpp file for more details.
//*****************************************************************************
#pragma once

// Class used in linked list for ListDuplicate
class Node
{
private:
	Node();		// private constructor to ensure all creation goes through 'Allocate' function
	~Node();	// private destructor to ensure all deletion goes through 'Deallocate' function
	static Node* LinkedListDuplicateValues(Node* inputList);
	static Node* LinkedListDuplicateRandomNodes(Node* inputList, Node* outputList);

public:
	// Public member variables
	long Value;
	Node* NextNode;
	Node* RandomNode; // can be NULL, will not be self

	// Static helper functions - these methods should be used for any
	// allocating & deallocating of Node structures
	static Node* Allocate(void);
	static void Deallocate(Node* node);

	// Creates a deep copy of the input list
	static Node* LinkedListDuplicate(Node* inputList);
};