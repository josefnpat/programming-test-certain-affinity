//*****************************************************************************
// Question: SimulateLife
//
// For this problem, you will be writing a function to compute the state of a
// grid of cellular automata, and simulate the state of the grid as time
// progresses according to a rule set for the birth & death of cells.  Think
// of the classic mathematical "game of life" by John Conway.  The cells are
// arranged in a rectangular grid.
//
// - Each cell can have as many as 8 neighbors (interior cells) and as few as 3
//   neighbors (the four corner cells).
// - Each cell can have one of two possible states:
//
// 1. alive (occupied)
// 2. dead (empty)
//
// Over the course of the life simulation for your grid, time passes in
// discrete increments.  The state of a cell at time N+1 is determined by the
// state of its neighboring cells at time N.  For example, in one variation of
// this simulation, an empty cell becomes 'alive' if exactly 3 of its neighbors
// were 'alive' in the previous time step, and an occupied cell 'dies' if it
// was surrounded by either more than 3 or less than 2 occupied cells. If the
// state of the cellular automata grid is identical across two consecutive time
// steps then it has reached a steady state.
//
// To solve this problem, you will write code to determine the state of a
// cellular automata grid at some number of time steps into the future.  The
// prototype for the function you will write is:
//
// int SimulateLife(
//     bool *cellularAutomataGrid,
//     int gridWidth,
//     int gridHeight,
//     int birthRules,
//     int deathRules,
//     int numberOfTimeSteps);
//
// Where:
//
// * cellularAutomataGrid is an array of (gridWidth x gridHeight) cells,
// with some initial population when this function is called.
// A value of 1 or 'true' means 'alive' (occupied); 0 / 'false' means 'dead' (empty).
// For example, the following array: [1, 0, 1, 0, 1, 0, 1, 0, 1] with gridWidth==3 and gridHeight==3
// would correspond to a cellular automata grid which has the following 2-D representation:
//
// 1 0 1
// 0 1 0
// 1 0 1
//
// * gridWidth is the width (pitch) of the array of cells
// * gridHeight is the height (row count) of the array of cells
// * birthRules is a bitmask used to determine when empty cells become alive (see below)
// * deathRules is a bitmask used to determine when occupied cells become empty (see below)
// * numberOfTimeSteps is the number of iterations of the life simulation that you should perform the life simulation on the cellularAutomataGrid
// * the function returns the number of time steps of life simulation actually performed,
//   which should be equal to numberOfTimeSteps unless the simulation reaches a steady state before then,
//   in which case it will may be some value less than numberOfTimeSteps
// * upon exit, cellularAutomataGrid should represent the state of the life simulation after the number of executed time steps
//
// The rules for birth and death are generalized as bit-masks.
// An empty cell with X occupied neighbors becomes alive in the next time step if (birthRules & (1<<X))!=0.
// An occupied cell with Y occupied neighbors dies in the next time step if (deathRules & (1<<Y))!=0.
// Otherwise, the cell retains its previous state from one time step to the next.
//
// For example, in the version of the simulation described previously, birthRules would be 0x0008 and deathRules would be 0x01F3.
//*****************************************************************************

#include <iostream>
using namespace std;

int SimulateLife(
	bool *cellularAutomataGrid,
	int gridWidth,
	int gridHeight,
	int birthRules,
	int deathRules,
	int numberOfTimeSteps)
{

	// Since SimulateLife is not a part of a class we declare the
	// following "anonymous" function so we can provide some extra
	// clarity.
	struct Local {
		// Simply calculate the index of a 2D array if it operated on a 1D array
		static int getIndex(int x, int y, int width) {
			return width*x + y;
		}
	};

	for (int step = 0; step < numberOfTimeSteps; step++) {

		// Determines if the last grid was the same as this grid.
		bool steady_change = false;

		// Since changes in life are to be in the upcoming version (N+1) the current
		// version (N) can't be changed. Here we have an auxilary grid.
		bool *newGrid = new bool[gridWidth*gridHeight];

		// The number of alive neighbors for a cell
		int alive;

		for (int x = 0; x < gridWidth; x++) {
			for (int y = 0; y < gridHeight; y++) {

				int index = Local::getIndex(x, y, gridWidth);
				// Initialize the grid value with the current value
				newGrid[index] = cellularAutomataGrid[index];

				alive = 0;
				//cout << "[" << x << "," << y << "]\n";

				// This could be unrolled for performance to avoid the 0,0 offset check,
				// but we won't in case we want a larger offeset.
				for (int offx = -1; offx <= 1; offx++) {
					for (int offy = -1; offy <= 1; offy++) {

						// Target X and Y pushed into memory so we don't have to 
						// Keep calculating it over and over.
						int tx = x + offx;
						int ty = y + offy;

						if ( // valid cell
							tx >= 0 && // keep x on the board
							tx < gridWidth &&
							ty >= 0 && // keep y on the board
							ty < gridHeight &&
							(offx != 0 || offy != 0) // ignore center
							) {
							if (cellularAutomataGrid[Local::getIndex(tx, ty, gridWidth)]) {
								alive++;
							}

						} // valid cell

					} // offy
				} // offx


				if ((birthRules & (1 << alive)) != 0) {
					if (newGrid[index] == false) {
						newGrid[index] = true;
						steady_change = true;
					}
				}

				if ((deathRules & (1 << alive)) != 0) {
					if (newGrid[index] == true) {
						newGrid[index] = false;
						steady_change = true;
					}
				}


			} // y
		} // x

		// Copy the contents of new grid into the input grid
		for (int i = 0; i < gridWidth*gridHeight; i++) {
			cellularAutomataGrid[i] = newGrid[i];
		}

		// If there are no changes, return next step
		if (steady_change == false) {
			return step+1;
		}
	}

	// All steps run, return total steps
	return numberOfTimeSteps;
}
