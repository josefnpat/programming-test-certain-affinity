//*****************************************************************************
// Question: MultiplyBySeven
//
// Write a function which takes as input an integer number and returns that
// number multiplied by seven (7).  For example, if the input is 3, a call to
// this function would return 21.  The only constraints to your function are:
// 1) You cannot use multiplication or division operators (ie *, *=, /, /=).
//    Assume you are developing on a platform with no multiply instruction.
// 2) You cannot call any other functions
//
//
// input: an integer number
// output: the input number multiplied by seven (7)
//
// for example:
//    long threeTimesSeven = MultiplyBySeven(3);
//    assert(threeTimesSeven == 21);
//*****************************************************************************
#pragma once

inline int MultiplyBySeven(int input)
{
	/*  Since we don't have any multiplication or division operators availible on
	this instruction set, the best course of action is to use a logical shift left.
	We will need to make some powers of two availble to us:

	output = input*7
	output = input*(8-1)
	output = (input*8)-(input*1)
	output = 8*input-input
	output = (2^3)*input-input
	output = (bitshift input left by 3) - input
	*/

	// Multiply the input by eight using a logical shift left then subtract the input from the result
	return (input << 3) - input;
}
