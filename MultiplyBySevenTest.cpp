//*****************************************************************************
// Tests for MultiplyBySeven
//*****************************************************************************

#include <cassert>

#include "MultiplyBySeven.h"

void MultiplyBySevenTest()
{
	// Here we use a variety of numbers. We won't test for overflow here.
	for (int i = -1000; i < 1000; i++) {
		assert(i * 7 == MultiplyBySeven(i));
	}
}