//*****************************************************************************
// Tests for DuplicateTest
//*****************************************************************************

#include <cassert>
#include <iostream>
using namespace std;

#include "ListDuplicate.h"

void ListDuplicateTest()
{
	// Here we have a complicated structure to test with:
	/*

	A quick overview, ascii style:

	one (1) ---> two (2) ---> three (3) ---> four (4) ---> doublefour (4)
	   \(r)        \(r)       /                            /
	    \___________\ _______/                            /
		             \___________________________________/

	Note: we test specifically if two(r) is pointing at doublefour, and not four.

	*/

	Node* doublefour;
	doublefour = doublefour->Allocate();
	doublefour->Value = 4;

	Node* four;
	four = four->Allocate();
	four->Value = 4;
	four->NextNode = doublefour;

	Node* three;
	three = three->Allocate();
	three->Value = 3;
	three->NextNode = four;

	Node* two;
	two = two->Allocate();
	two->Value = 2;
	two->NextNode = three;
	two->RandomNode = doublefour;

	Node* one;
	one = one->Allocate();
	one->Value = 1;
	one->NextNode = two;
	one->RandomNode = three;

	Node *newList = one->LinkedListDuplicate(one);

	// Anonymous function for printing node lists just in case we need to debug stuff
	struct LocalListDuplicate {
		static void printLinkedList(Node* inputList) {
			Node* currentInputNode = inputList;
			while (currentInputNode != NULL) {
				cout << "v:" << currentInputNode->Value;
				if (currentInputNode->RandomNode) {
					cout << "(rv:" << currentInputNode->RandomNode->Value << ")";
				}
				if (currentInputNode->NextNode != NULL) {
					cout << ",";
				}
				currentInputNode = currentInputNode->NextNode;
			}
			cout << std::endl;
		}
	};

	//LocalListDuplicate::printLinkedList(one);
	//LocalListDuplicate::printLinkedList(newList);

	// Ensure newList has data
	assert(newList != NULL);

	// Test first element
	assert(newList->Value == 1);
	assert(newList->RandomNode != NULL);
	assert(newList->RandomNode->Value == 3);
	assert(newList->RandomNode == newList->NextNode->NextNode); // Ensure that the random node points to the third element
	assert(newList->RandomNode != one->RandomNode); // Ensure that the pointer is not the same as the original
	assert(newList != one); // Ensure that the first elemenets are not the same pointer

							// Test second element
	assert(newList->NextNode->Value == 2);
	// Ensure that the random node points to the fifth element (doublefour) and not the fourth element (four)
	assert(newList->NextNode->RandomNode == newList->NextNode->NextNode->NextNode->NextNode);
	assert(newList->NextNode != one->NextNode); // Ensure that the second elements are not the same pointer

												// Test third element
	assert(newList->NextNode->NextNode->Value == 3);
	assert(newList->NextNode->NextNode->RandomNode == NULL);
	assert(newList->NextNode->NextNode != one->NextNode->NextNode); // Ensure that the third elements are not the same pointer

																	// Test fourth element
	assert(newList->NextNode->NextNode->NextNode->Value == 4);
	assert(newList->NextNode->NextNode->NextNode->RandomNode == NULL);
	assert(newList->NextNode->NextNode->NextNode != one->NextNode->NextNode->NextNode); // Ensure that the fourth elements are not the same pointer

																						// Text fifth element
	assert(newList->NextNode->NextNode->NextNode->NextNode->Value == 4);
	assert(newList->NextNode->NextNode->NextNode->NextNode->RandomNode == NULL);
	assert(newList->NextNode->NextNode->NextNode->NextNode->NextNode == NULL);
	assert(newList->NextNode->NextNode->NextNode->NextNode != one->NextNode->NextNode->NextNode->NextNode); // Ensure that the fifth elements are not the same pointe

}