//*****************************************************************************
// Question: ReverseWords
//
// Write the following function to reverse the words in a text string,
// using an algorithm which is as time and space constrained as possible.
// You may use any C/C++ standard library string manipulation functions you
// wish.
//
// input: a character string consisting of words separated by whitespace
// output: the same string but with the words reversed,
//
// For example:
//
// char sentence[]= "My name is Chris";
// printf("%s\n", sentence);
// ReverseWords(sentence);
// printf("%s\n", sentence);
//
// would output:
// My name is Chris
// Chris is name My
//*****************************************************************************
#include "ReverseWords.h"
#include <iostream>
using namespace std;

void ReverseWords(char* sentence)
{
	// anonymous function to reverse segments of a string
	struct Local {
		static void reverseStringSegment(char* segment,int starti,int len) {
			// Only flip the first half against the second half
			for (int i = 0; i < len / 2; i++) {
				swap(segment[starti + i], segment[starti + len - i - 1]);
			}
		}
	};

	int len = strlen(sentence);

	// find and reverse individual words
	int word_start = 0;
	bool check_leading_space = true;
	for (int i = 0; i < len; i++) {
		if (check_leading_space) {
			if (sentence[i] == ' ') {
				word_start++;
			}
			else {
				check_leading_space = false;
			}
		}
		// If there is a sentinel character
		if(sentence[i] == ' ' || i == len-1){
			int a = word_start;
			Local::reverseStringSegment(sentence, 
				word_start, 
				i - word_start + (i==len-1 ? 1 : 0) // add an offset if it's the last character
			);
			word_start = i+1;
		}
	}

	// reverse entire string
	for (int i = 0; i < len/2; i++) {
		swap(sentence[i], sentence[len - i - 1]);
	}

}
