This is Certain Affinity's take-home programming test.  It is meant to
test your understanding of basic C/C++ concepts as well as your algorithmic
design sense.  Answer each question as well as you can.  If you run into any
ambiguity or problems with a question, just include it in your answer and
move on.

The questions are in the .h and .cpp files under the "source" folder.  Please
answer them in the places provided.  For convenience, a Visual Studio 2013
project and solution are included, but don't have to be used.

- MultiplyBySeven.h
- ListDuplicate.h / .cpp
- SimulateLife.h / .cpp
- ReverseWords.h / .cpp

Things that will be considered when reviewing your solutions include:
	* Solutions that are bug-free and solve the given problems
	* Use of algorithms geared towards high performance and low resource usage.
	  For example, an O(n) time algorithm with O(1) additional memory requirement
	  is better than an O(n^2) time algorithm with O(n) memory requirement
	* Clear, concise code which is easy to read and understand

For the purposes of this test, all solutions should be written in C++.
Standard C/C++/C++11 library functions and containers may be used unless noted
otherwise.  Use of any platform-specific libraries or functions should be
avoided.  For each coding problem, you are provided with the prototype for the
function that serves as the interface to your solution.  You are responsible
for implementing this function and writing any supporting functions and test or
verification code.

You are encouraged to include with your solutions any notes, explanations,
documentation and test code that you might have written to support your
solutions.  Citing references is fine, but getting direct help on the problems
from others or the Internet is frowned upon.

There is no time limit on how quickly to return this completed test, but
obviously quicker is better. It is anticipated that completing the full test
will take a few hours. Send the completed test back to the email address from
which it was sent to you.

Thank you! 
