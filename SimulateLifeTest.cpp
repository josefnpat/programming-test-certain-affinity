//*****************************************************************************
// Test functions for SimulateLife
//*****************************************************************************

#include <cassert>
#include <iostream>
using namespace std;

#include "SimulateLife.h"

void SimulateLifeTest()
{

	// anonymous functions for testing
	struct LocalSimulateLife {
		// show the board of life with X's and _'s
		static void showLife(bool *cellularAutomataGrid, int gridWidth, int gridHeight) {
			for (int i = 0; i < gridHeight*gridHeight; i++) {
				cout << (cellularAutomataGrid[i] ? "X" : "_");
				if (i%gridWidth == gridWidth - 1) {
					cout << std::endl;
				}
			}
		}
		// compare two grids
		static bool compareGrid(bool* gridA, bool* gridB, int gridWidth, int gridHeight) {
			for (int i = 0; i < gridHeight*gridWidth; i++) {
				if (gridA[i] != gridB[i]) {
					return false;
				}
			}
			return true;
		}
	};

	// Ensure that compareGrid works
	bool gridTest[9] = { 1,0,1,0,1,0,1,0,1 };
	assert(LocalSimulateLife::compareGrid(gridTest, gridTest, 3, 3));

	// GIVEN SAMPLE (star -> tub)
	//cout << "Sample (start->tub):" << std::endl;
	bool sample[3 * 3] = { 1,0,1, 0,1,0, 1,0,1 };
	bool sample_check[3 * 3] = { 0,1,0, 1,0,1, 0,1,0 }; // tub
	// Should start at start, switch to tub, then remain at tub, for two steps
	assert(SimulateLife(sample, 3, 3, 0x0008, 0x01f3, 10) == 2);
	assert(LocalSimulateLife::compareGrid(sample, sample_check, 3, 3));
	//LocalSimulateLife::showLife(sample, 3, 3);

	// STILL LIFE, BOAT
	//cout << "Still Life, Boat:" << std::endl;
	bool still_life_boat[3 * 3] = { 1,1,0, 1,0,1, 0,1,0 };
	bool still_life_boat_check[3 * 3] = { 1,1,0, 1,0,1, 0,1,0 };
	// Should start at boat, and remain at boat for one step.
	assert(SimulateLife(still_life_boat, 3, 3, 0x0008, 0x01f3, 10) == 1);
	assert(LocalSimulateLife::compareGrid(still_life_boat, still_life_boat_check, 3,3));
	//LocalSimulateLife::showLife(still_life_boat, 3, 3);

	// OSCILLATOR, TOAD
	bool oscillator_toad[4 * 4] = { 0,0,1,0, 1,0,0,1, 1,0,0,1, 0,1,0,0 };
	//cout << "Oscillator, Toad:" << std::endl;
	bool oscillator_toad_odd[4 * 4] = { 0,0,1,0, 1,0,0,1, 1,0,0,1, 0,1,0,0 };
	bool oscillator_toad_even[4 * 4] = { 0,0,0,0, 0,1,1,1, 1,1,1,0, 0,0,0,0 };
	// Test to see that we get the odd pattern
	assert(SimulateLife(oscillator_toad, 4, 4, 0x0008, 0x01f3, 10) == 10);
	assert(LocalSimulateLife::compareGrid(oscillator_toad, oscillator_toad_odd, 4, 4));
	// Test to see that we get the even pattern
	assert(SimulateLife(oscillator_toad, 4, 4, 0x0008, 0x01f3, 9) == 9);
	assert(LocalSimulateLife::compareGrid(oscillator_toad, oscillator_toad_even, 4, 4));
	//LocalSimulateLife::showLife(oscillator_toad, 4,4);

}