//*****************************************************************************
// Question: SimulateLife
// Implement SimulateLife() function in "SimulateLife.cpp".
// See the .cpp file for more details.
//*****************************************************************************
#pragma once

extern int SimulateLife(
	bool *cellularAutomataGrid,
	int gridWidth,
	int gridHeight,
	int birthRules,
	int deathRules,
	int numberOfTimeSteps);
