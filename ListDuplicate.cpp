//*****************************************************************************
// Question: ListDuplicate
//
// Write the body for the LinkedListDuplicate() function, whose purpose is to
// duplicate a singly-linked list which is passed as input into the function:
//
// Node *LinkedListDuplicate(Node *inputList);
//
// where inputList is a pointer to the first node in a singly-linked list,
// whose elements each contain an integer value and pointers to two other
// elements in the list: the next link in the list, and some other random
// element in the list. NOTE: these pointers could be NULL, but will never be
// itself (no self-referencing nodes).
//
// Given an input linked list, your function should return a functionally
// duplicate copy of the list that has no dependency on the original input
// list.  The definition of "functionally duplicate" is that the output list,
// if traversed in order, will contain nodes which, when examined, have the
// same 'Value' and values in their NextNode and RandomNode members (or NULL
// as appropriate) as the input list has if traversed in the same order.
//
// The linked-list node structure is defined in "ListDuplicate.h"...
//*****************************************************************************
#include <stdlib.h>
#include "ListDuplicate.h"

//-----------------------------------------------------------------------------
// Creates a deep copy of the input list
//-----------------------------------------------------------------------------
Node* Node::LinkedListDuplicate(Node* inputList)
{
	// We could have combined these two functions, but if the RandomNode is 
	// ever removed, this makes it easy to clean up. On top of that, the code might
	// get a little unclear if we nested it all.
	Node* outputList = LinkedListDuplicateValues(inputList);
	return LinkedListDuplicateRandomNodes(inputList,outputList);
}
//-----------------------------------------------------------------------------
// Creates a deep copy of the input list with only NextNode and Values populated
//-----------------------------------------------------------------------------
Node* Node::LinkedListDuplicateValues(Node* inputList)
{
	Node* currentInputNode = inputList;
	Node* outputList = NULL;
	Node* currentOutputNode;
	while (currentInputNode != NULL) {
		Node* newNode;
		newNode = newNode->Allocate();
		newNode->Value = currentInputNode->Value;
		if (outputList == NULL) { // If first element
			outputList = newNode;
		}
		else { // first element exists, use current node
			currentOutputNode->NextNode = newNode;
		}
		currentOutputNode = newNode;
		// Move to next node (or NULL)
		currentInputNode = currentInputNode->NextNode;
	}
	return outputList;
}

//-----------------------------------------------------------------------------
// Iterate over the outputList and set up the RandomNode pointers
//-----------------------------------------------------------------------------
Node* Node::LinkedListDuplicateRandomNodes(Node* inputList,Node* outputList)
{
	// We iterate trough both lists here so we don't have to count jumps forward,
	// or worse; jumps backwards.
	Node* curInput = inputList;
	Node* curOutput = outputList;

	// While the instructions say to assume that the Random node (if not NULL) is
	// in the list, from the code provided, it could very easily be an unrelated node.
	while (curInput != NULL) {
		if (curInput->RandomNode) {
			Node* subInput = inputList;
			Node* subOutput = outputList;
			while (subInput != NULL) {
				if (curInput->RandomNode == subInput) {
					// associate the output node from how the input node is related
					curOutput->RandomNode = subOutput;
					break; // stop searching
				}
				subInput = subInput->NextNode;
				subOutput = subOutput->NextNode;
			}
		}
		curInput = curInput->NextNode;
		curOutput = curOutput->NextNode;
	}
	return outputList;
}

//-----------------------------------------------------------------------------
// Node constructor initializes Value to zero and node pointers to NULL
//-----------------------------------------------------------------------------
Node::Node() :
Value(0),
NextNode(NULL),
RandomNode(NULL)
{
}

//-----------------------------------------------------------------------------
// Node destructor cleans up everything that follows it
//-----------------------------------------------------------------------------
Node::~Node()
{
	// Nodes are responsible for deleting their NextNode, but not the RandomNode
	if (NextNode != NULL)
	{
		delete NextNode;
	}
}

//-----------------------------------------------------------------------------
// Allocates and returns a new node.
//-----------------------------------------------------------------------------
Node* Node::Allocate(void)
{
	return new Node();
}

//-----------------------------------------------------------------------------
// Frees memory associated with a node created with Allocate.
//-----------------------------------------------------------------------------
void Node::Deallocate(Node *node)
{
	delete node;
}