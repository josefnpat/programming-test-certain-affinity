//*****************************************************************************
// Question: ReverseWords
//
// Implement ReverseWords() function in "ReverseWords.cpp".
// See the .cpp file for more details.
//*****************************************************************************
#pragma once

extern void ReverseWords(char* sentence);
