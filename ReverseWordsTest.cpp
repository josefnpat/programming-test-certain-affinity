//*****************************************************************************
// Tests for ReverseWords
//*****************************************************************************

#include <cassert>
#include <iostream>
using namespace std;

#include "ReverseWords.h"

void ReverseWordsTest()
{

	// Test the example we were given
	char sampleSentence1[] = "My name is Chris";
	ReverseWords(sampleSentence1);
	assert(strcmp(sampleSentence1, "Chris is name My") == 0);

	// Make sure it works with leading space
	char sampleSentence2[] = " Hello World";
	ReverseWords(sampleSentence2);
	assert(strcmp(sampleSentence2, "World Hello ") == 0);

	// Test a single word
	char sampleSentence3[] = "Linux";
	ReverseWords(sampleSentence3);
	assert(strcmp(sampleSentence3, "Linux") == 0);

	// Test a complex sentence
	char sampleSentence4[] = "Let's play some video games today, what do you say?:wq";
	ReverseWords(sampleSentence4);
	assert(strcmp(sampleSentence4, "say?:wq you do what today, games video some play Let's") == 0);

}